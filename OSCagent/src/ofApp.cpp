#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
    
    ofSetLogLevel( OF_LOG_FATAL_ERROR );
    
    std::bitset<4> flagA( int( 1 ) );
    std::bitset<4> flagB( int( 2 ) );
    std::bitset<4> flagC( int( 4 ) );
    std::bitset<4> flagD( int( 8 ) );
    
    std::bitset<4> x( uint16_t( 0 ) );
    x |= flagA;
    x |= flagD;
    
    uint16_t v = (uint16_t) x.to_ulong();
    cout << "int: " << v << endl;
    
    std::bitset<4> bs( 17 );
    
    cout << "bitset: " << bs[0] << ", " << bs[1] << ", " << bs[2] << ", " << bs[3] << endl;
    
    std::bitset<4> cp;
    for ( int i = 0; i < 4; ++i ) {
        cp[ i ] = bs[ i ];
    }
    
    cout << "bitset copy: " << bs[0] << ", " << bs[1] << ", " << bs[2] << ", " << bs[3] << endl;
    
    bs[1] = 1;
    
    if (   
        cp[ 0 ] == bs[ 0 ] &&
        cp[ 1 ] == bs[ 1 ] &&
        cp[ 2 ] == bs[ 2 ] &&
        cp[ 3 ] == bs[ 3 ]
    ) {
        cout << "cp and bs are identical" << endl;
    } else {
        cout << "cp and bs are different" << endl;
    }
    
    DataKind dk = OSCAgent::makeKind( false, false, false, false );
    oscagent = new OSCAgent( dk );
    oscagent->addListener( this );
    
}

//--------------------------------------------------------------
void ofApp::update(){

    oscagent->update( ofGetElapsedTimeMillis() );
    
}

void ofApp::process( ofxOscMessage & msg ) {
    
    if ( msg.getAddress().compare( OSC_ADDRESS_DATA ) == 0 ) {

        DataKind dk( msg.getArgAsInt( 0 ) );
        
        if ( !dk[ 0 ] && !dk[ 1 ] && !dk[ 2 ] && !dk[ 3 ] ) {
            // not a tag!
            return;
        }
        
        string sender_name = msg.getArgAsString( 1 );
        int tagid = msg.getArgAsInt( 2 );
        int event = msg.getArgAsInt( 3 );
        
        stringstream ss;
        ss << msg.getRemoteIp() << "-t-" << tagid;
        string utag = ss.str();
        
        if ( !dk[ 0 ] && !dk[ 1 ] && !dk[ 2 ] && dk[ 3 ] ) {
            
            cout << "matrix received" <<endl;
            
            if ( event == TAG_DISAPPEARED && mats.find( utag ) != mats.end() ) {
                
                mats.erase( utag );
                cout << "matrix removed" <<endl;
                
            } else {
                
                if ( mats.find( utag ) == mats.end() ) {
                    mats[ utag ] = ofMatrix4x4();
                }
                for ( int i = 0; i < 16; ++i ) {
                    mats[ utag ].getPtr()[ i ] = msg.getArgAsFloat( i + 4 );
                }
                ofVec3f t = mats[ utag ].getTranslation();
                cout << "\ttranslation: " << t.x << ", " << t.y << ", " << t.z << endl;
            
            }
            
        } else if ( !dk[ 0 ] && dk[ 1 ] && dk[ 2 ] && dk[ 3 ] ) {
            
            cout << "xyz, z rotation and matrix received" <<endl;
            
            if ( event == TAG_DISAPPEARED && alldatas.find( utag ) != alldatas.end() ) {
                
                alldatas.erase( utag );
                cout << "all data " << utag << " removed" <<endl;
                
            } else {
            
                alldatas[ utag ].tagid = tagid;
                alldatas[ utag ].xyz.set(
                    msg.getArgAsFloat( 4 ),
                    msg.getArgAsFloat( 5 ),
                    msg.getArgAsFloat( 6 )
                );
                alldatas[ utag ].zrotation = msg.getArgAsFloat( 7 );
                for ( int i = 0; i < 16; ++i ) {
                    alldatas[ utag ].mat.getPtr()[ i ] = msg.getArgAsFloat( i + 8 );
                }
                // alldatas[ utag ].mat.getRotate().
                
            }
            
        }
        
    }

}

void ofApp::newAgent( string unique, DataSender & sender ) {

    if ( !sender.kind[ 3 ] ) {
        return;
    }
    
//    DataKind dk = OSCAgent::makeKind( false, false, false, true );
    DataKind dk = OSCAgent::makeKind( false, true, true, true );
    oscagent->request( unique, dk.i );
    cout << "sending request to " << unique << ", " << int( dk.i ) << endl;
    
}

void ofApp::deletedAgent( string unique ) {

}    

//--------------------------------------------------------------
void ofApp::draw(){

    ofBackground( 25,25,25 );
    
    ofPushMatrix();
    ofTranslate( ofGetWidth() * 0.5, ofGetHeight() * 0.75, -500 );
//    ofRotateY( ofGetFrameNum() * 0.1 );
//    ofRotateX( ofGetFrameNum() * 0.2 );
    ofRotateX( 120 );
    
    ofSetLineWidth( 1 );
    ofSetColor( 255,0,0 );
    ofLine( 0,0,0, 100,0,0 );
    ofSetColor( 0,255,0 );
    ofLine( 0,0,0, 0,100,0 );
    ofSetColor( 0,0,255 );
    ofLine( 0,0,0, 0,0,100 );
    
    ofNoFill();
//    ofSetLineWidth( 5 );
    
    for ( map< string, ofMatrix4x4 >::iterator itm = mats.begin(); itm != mats.end(); ++itm ) {
        ofSetColor( 255,255,0 );
        ofVec3f t = itm->second.getTranslation();
        ofLine( 0,0,0, t.x, t.y, t.z );
        ofPushMatrix();
        ofMultMatrix( itm->second );
        ofDrawBox( 0,0,0, 100 );
        ofSetColor( 255,0,0 );
        ofLine( 0,0,0, 100,0,0 );
        ofSetColor( 0,255,0 );
        ofLine( 0,0,0, 0,100,0 );
        ofSetColor( 0,0,255 );
        ofLine( 0,0,0, 0,0,100 );    
        ofPopMatrix();
    }
    
    for ( map< string, AllData >::iterator itm = alldatas.begin(); itm != alldatas.end(); ++itm ) {
        ofSetColor( 255,255,0 );
        ofVec3f t = itm->second.xyz;
        ofLine( 0,0,0, t.x, t.y, t.z );
        ofSetColor( 255,255,255 );
        ofDrawBitmapString( ofToString( itm->second.tagid ), t.x, t.y, t.z );
        ofPushMatrix();
            ofMultMatrix( itm->second.mat );
            ofSetColor( 255,255,0 );
            ofDrawBox( 0,0,0, 100 );
            ofSetColor( 255,0,0 );
            ofLine( 0,0,0, 100,0,0 );
            ofSetColor( 0,255,0 );
            ofLine( 0,0,0, 0,100,0 );
            ofSetColor( 0,0,255 );
            ofLine( 0,0,0, 0,0,100 );
        ofPopMatrix();
    }
    
    ofPopMatrix();
    
    for ( map< string, AllData >::iterator itm = alldatas.begin(); itm != alldatas.end(); ++itm ) {
        ofPushMatrix();
            ofTranslate( ofGetWidth() * 0.5, ofGetHeight() * 0.5, 0 );
            ofRotateZ( itm->second.zrotation );
            ofSetColor( 255,255,255, 80 );
            ofSetLineWidth( 3 );
            ofLine( 0,0,0, 250,0,0 );
            ofDrawBitmapString( ofToString( itm->second.tagid ) + ", rot z " + ofToString( itm->second.zrotation ), 250,0,0 );
            ofSetLineWidth( 1 );
        ofPopMatrix();
    }
    
    ofSetColor( 255,255,255 );
    ofDrawBitmapString( oscagent->toString(), 10, 25 );
    
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}