#pragma once

#include "ofMain.h"
#include "OSCAgent.h"

struct AllData {
    int tagid;
    ofVec3f xyz;
    float zrotation;
    ofMatrix4x4 mat;
};

class ofApp : public ofBaseApp, public OSCagentListener {
public:
    
    void setup();
    void update();
    void draw();

    void keyPressed(int key);
    void keyReleased(int key);
    void mouseMoved(int x, int y);
    void mouseDragged(int x, int y, int button);
    void mousePressed(int x, int y, int button);
    void mouseReleased(int x, int y, int button);
    void mouseEntered(int x, int y);
    void mouseExited(int x, int y);
    void windowResized(int w, int h);
    void dragEvent(ofDragInfo dragInfo);
    void gotMessage(ofMessage msg);
 
    void process( ofxOscMessage & msg );
    void newAgent( string unique, DataSender & sender );
    void deletedAgent( string unique );
    
private:
    
    OSCAgent * oscagent;
    
    map< string, ofMatrix4x4 > mats;
    map< string, AllData > alldatas;
    
};
