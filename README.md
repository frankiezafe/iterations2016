Iterations II
=============

Applications
------------

+ To configure, start and use the Raspberry Pis, see the [Configuration](#configuration) section of this page.
+ [Name and roles](#roles) of the points in the big room @imal.
+ Go to [development notes](#dev) for all technical details

Configuration
=============

### Network Map
```
10.0.0.11 francois.local
10.0.0.12 julien.local
10.0.0.13 reni.local
10.0.0.14 claire.local
10.0.0.15 peter.local
10.0.0.16 pascale.local
10.0.0.17 miriam.local
10.0.0.18 annie.local
10.0.0.19 donatella.local
10.0.0.20 antoine.local
10.0.0.21 baptiste.local
10.0.0.22 femke.local
10.0.0.23 rafaella.local
10.0.0.24 tom.local
10.0.0.25 stefan.local
```

Some routers acting as switches:  
(in case they need to be configured differently)  
```
10.0.0.1
10.0.0.2
```
Map
---
![Network map](network-vernissage.jpg)


Roles
-----

In the François's navigation order.

**10.0.0.16 pascale.local**
+ video player on the wall in the entrance

**10.0.0.18 annie.local**
+ motion detection pointing at the bar
+ sound player, speaker on the pillar

**10.0.0.22 femke.local**
+ tag detection on the floor near technical room's door
+ sound player, speaker hanging for the ceiling

**10.0.0.15 peter.local**
+ tag detection at the second window
+ text player, screen on the wall

**10.0.0.21 baptiste.local**
+ tag detection (table with stickies)
+ sound player

**10.0.0.12 julien.local**
+ tag detection on the floor
+ tex player, screen on the floor

**10.0.0.19 donatella.local**
+ tag detection on the floor, near the exit door
+ web player, screen on the wall

**10.0.0.23 rafaella.local**
+ video player on the floor

**10.0.0.11 francois.local**
+ distorted map on the wall

**10.0.0.25 stefan.local**
+ tag detection on the floor, in front of the map
+ tex player, screen on the pillar

**10.0.0.17 miriam.local**
+ sound player, speaker on the pillar

### Monitor room
**antoine.local**
+ No network, ip static
+ Displays gpredict

**reni.local**
+ No network, ip static
+ Plays interview videos

**claire.local**
+ No network, ip static
+ Plays shipmap video

**tom.local**
+ Internet, DHCP
+ Displays browser with Internetcablemap

How to access and upload files to a raspberry
---------------------------------------------

### Other method

scan the network to find raspberry's IP, using a command like this (depending on the network configuration)

  ```
  $ nmap -F 10.0.0.10-30
  ```

example output:

  ```
  Nmap scan report for 10.0.0.12
  Host is up (0.000057s latency).
  Not shown: 996 closed ports
  PORT    STATE SERVICE
  80/tcp  open  http
  81/tcp  open  hosts2-ns
  139/tcp open  netbios-ssn
  445/tcp open  microsoft-ds

  Nmap scan report for 10.0.0.13
  Host is up (0.043s latency).
  Not shown: 999 closed ports
  PORT   STATE SERVICE
  22/tcp open  ssh

  Nmap scan report for 10.0.0.14
  Host is up (0.12s latency).
  All 1000 scanned ports on 10.0.0.104 are closed

  Nmap scan report for 10.0.0.16
  Host is up (0.029s latency).
  Not shown: 999 closed ports
  PORT   STATE SERVICE
  22/tcp open  ssh
  ```

Raspberries will typically have only 1 port open, the **22/tcp**

Each raspberry have its own name. To check it, just connect in ssh by doing:

  ```
  $ ssh alarm@10.0.0.12
  ```

enter the password, and the name of the machine will appears:

  ```
  [alarm@julien ~]$
  ```

### Putting files

You are certain that the raspberry is the good one.

You are ready to connect in **sftp** to the raspberry.

To do so,

+ open your file browser
+ type **sftp://alarm@10.0.0.12** in the address bar
+ enter the password
+ iterations files are located in a folder called **/home/alarm/of.../apps/iterations2016/**

To add or remove videos, sounds or texts from the mediaplayer, go to **/home/alarm/of.../apps/iterations2016/MediaPlayer/bin/data/**.

Dev
===

### Usefull

+ [MediaPlayer](#mediaplayer) - versatile player that plays videos, sounds and display texts
+ [Iteration](#iteration) - the tag detection
+ web - node/html application showing the network activity
+ MapDistortion - deformations of a map

### Devs

+ ARtoolkit - test for ARTollKit
+ OpenGLtest - test for opengl on raspberry
+ OSCAgent - shows the tags seen in the network with ID and 3D positions

### Scripts

+ iteration2016_startup_script.sh - startup for raspberries
+ iterations.crontab - shortcut to register iteration2016_startup_script.sh at boot
+ web.sh - startup script to launch midori with a custom address
+ tags/tagger.sh - generate tags ready to print (pdf format)

Archlinux
---------

We worked with [ArchLinuxARM](https://archlinuxarm.org/).

See [installation instructions](https://archlinuxarm.org/platforms/armv6/raspberry-pi).

We added several tools that make the work easier:

1. Basic tools  
  ```
  $ su
  $ pacman -Syu sudo git wget screen base-devel
  ```  
  `sudo` needs to be configured with `EDITOR=nano visudo`. To do so, uncomment line that says `%wheel ALL=(ALL) ALL`  
  See [How To Use Linux Screen](https://www.rackaid.com/blog/linux-screen-tutorial-and-how-to/) for options.  
  ` $ exit ` to exit `su` (super user).  
  Configure git if you want to commit
  ```
  $ git config --global user.email "you@example.com"
  $ git config --global user.name "Your Name"
  ```
2. Cron handler  
  ```
  $ sudo pacman -Syu cronie  
  $ systemctl enable cronie
  ```  
  Set a crontab file with this line:  
  `@reboot /usr/bin/sh ~/scripts/iteration2016_startup_script.sh`  
  Or do `contab scripts/iterations.crontab`

3. All the web stuff related stuff and kiosk browser  
  ```
  $ sudo pacman -Syu nodejs npm midori matchbox-window-manager xorg-xinit xorg-server xorg-server-utils xf86-video-fbdev xterm python2
  $ sudo ln -s /usr/bin/python2 /usr/bin/python
  ```  
  The symbolic link to Python2 is necessary to compile some modules for node.


Midori
------

+ `$ sudo ln -s /etc/fonts/conf.avail/70-no-bitmaps.conf /etc/fonts/conf.d/` to fix bitmap fonts display issue, src: https://wiki.archlinux.org/index.php/Midori
+ **WARNING**: for javascript, midori uses ecmascript 262 norm, and therefore several javascript functionalities are not available, such as Array.prototype.findIndex()...


Webcam
------

To test the webcam, install and use `fswebcam`.  
You will need to add the user `alarm` to the group `video` or use it as `root`.  
`$ sudo usermod -aG video alarm`

Sound
-----
To get sound playing with alsa, it requires `libmikmod` and adding the user `alarm` to the sound group `audio`.
Testing sound can be done with `alsaplayer`. It might be required to have also `alsa-utils`.

```
$ sudo pacman -Syu alsaplayer alsa-utils libmikmod
$ sudo usermod -aG audio alarm
```

Screen resolution
-----------------
To force screen resolution on screen with HDMI>VGA cables:
`$ /opt/vc/bin/tvservice -e "DMT 16 HDMI"`

[Set up /boot/config.txt](https://www.raspberrypi.org/documentation/configuration/config-txt.md)

Here is what we used:
```
# Tell Raspi that screen is attached
hdmi_force_hotplug=1
# Tell him to ignore info from screen
hdmi_ignore_edid=0xa5000080
# Set monitor mode to DMT
hdmi_group=2
# Set monitor resolution to 1024x768 XGA 60Hz (HDMI_DMT_XGA_60)
hdmi_mode=16
```

How to Install oF on RPi1&2
---------------------------
RTFM (especially for RPi2)

Openframeworks on RPi1&2 needs rtaudio (4.1.1) and rtmidi to be compiled by hand.
You'll find the PKGBUILD here:
+ http://pkgbuild.com/git/aur-mirror.git/tree/rtmidi/PKGBUILD
+ http://pkgbuild.com/git/aur-mirror.git/tree/rtaudio/PKGBUILD



Openframeworks addons
---------------------

+ [ofxARtoolKitPlus](https://github.com/fishkingsin/ofxARtoolkitPlus)
+ [ofxOMXPlayer lightweight](https://gitlab.com/frankiezafe/ofxOMXPlayer), based on [ofxOMXPlayer by jvcleave](https://github.com/jvcleave/ofxOMXPlayer)
+ [ofxOscMesh](https://github.com/frankiezafe/ofxOscMesh)


Tag detection
-------------

The tracking system used in this project is [ARToolKit](http://artoolkit.org/). It is not used for virtual reality but only as a *tag* recognition and positioning system.


Producing tags
--------------

to ouput printable tags, use the sh script located in **scripts/tags/**

open it in a text editor and adapt:

+ start_at
+ stop_at
+ to change output format, see line 69 and change A4

then run it:

  ```
  $ bash tagger.sh
  ```

and get your pdf at scripts/tags/toprint/alltags.pdf

the name of the pngs contains the tag id (tag_00**10**.png)


Iteration
---------

The program detect and send tags. To start it:
```
$ cd Iteration/bin/
$ ./Iteration
```

arguments
---------
+ *-verbose* to trace what's happening in the program
+ *-s 320x240* to set the webcam resolution
+ *-n [name]* to set the name of the point

Check the console after starting the program for other params.
```
'-nonet' to disable network
'-n [string]' to set the name of the point
'-p [int]' to set local port, 23000 by default
'-b [int]' to set broadcast port, 20000 by default
'-s [int]x[int]' to set the camera resolution, 160x120 by default
'-t [int]' to set the threshold, 85 by default
'-f [string|string]' to define the names of points listened
'-verbose
```

MediaPlayer
-----------
`$ ./MediaPlayer/bin/MediaPlayer`
### Arguments
```
'-nonet' to disable network
'-n [string]' to set the name of the point
'-p [int]' to set local port, 23000 by default
'-b [int]' to set broadcast port, 20000 by default
'-f [string|string]' to define the names of points listened
'-m [int]' set to 1 to enable keyboard control
'-txt [string]' to define the text file to load, path relative to data/
'-sf [int]' to define font size
'-cf [int],[int],[int],[int]' to define font color (RGBA), alpha is optional
'-ct [int],[int],[int],[int]' to define comments color (RGBA), alpha is optional
'-ci [int],[int],[int]' to define (idle) background color when nothing is displayed
'-cb [int],[int],[int]' to define background color when media is playing
'-verbose
```

Shipmap
-------
How to turn http://shipmap.org into a video:
1. Add this CSS to the page to hide extra info  
  ```
  #social,
  #info-panel,
  svg#play-button,
  .leaflet-control-container {
    display: none;
  }
  ```
2. Resize the Firefox Window using Developer Tools (CTRL+SHIFT+i) and launch FFmpeg at 1280x720 (HD720)  
  `$ ffmpeg -video_size 1280x720 -framerate 15 -f x11grab -i :0.0+20,138 shipmap.mp4`
3. Trim the resulting video  
  `$ ffmpeg -ss 00:00:20 -i shipmap.mp4 -an -qscale 1 -t 00:13:00 shipmap-ok.mp4`


Communication protocol
----------------------

see [ofxOscMesh readme](https://github.com/frankiezafe/ofxOscMesh/blob/master/README.md) for a complete description of the protocol.
