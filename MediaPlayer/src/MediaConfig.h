/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   MediaConfig.h
 * Author: frankiezafe
 *
 * Created on June 18, 2016, 6:09 PM
 */

#ifndef MEDIACONFIG_H
#define MEDIACONFIG_H

#include <string>
#include <string>
#include <sstream>
#include <vector>


class MediaConfig {

public:

    static std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems) {
        std::stringstream ss(s);
        std::string item;
        while (getline(ss, item, delim)) {
            elems.push_back(item);
        }
        return elems;
    }


    static std::vector<std::string> split(const std::string &s, char delim) {
        std::vector<std::string> elems;
        split(s, delim, elems);
        return elems;
    }

    static MediaConfig * get();
    
    float idle_red;
    float idle_green;
    float idle_blue;
    
    float bg_red;
    float bg_green;
    float bg_blue;
    
    float font_red;
    float font_green;
    float font_blue;
    float font_alpha;
    
    float trace_red;
    float trace_green;
    float trace_blue;
    float trace_alpha;
    
    float font_size;
    
    std::string text_path;
    
    bool manual;
    
    int lock_time;
    
private:
    
    MediaConfig();
    virtual ~MediaConfig();

};

#endif /* MEDIACONFIG_H */

