#include <sstream>

#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
    
    ofSetWindowTitle( "iteration media player" );
    
    if ( MediaConfig::get()->manual ) {
        cout << "manual controls enabled, use" << endl;
        cout << "\t's' to stop media" << endl;
        cout << "\t[any other key] to start media" << endl;
    } else {
        ofHideCursor();
    }
    
#ifdef TARGET_OPENGLES
    
    ofEnableNormalizedTexCoords();
    vplayer = nullptr;
    
#endif
    
    ofSetLogLevel( OF_LOG_FATAL_ERROR );
    DataKind dk = OSCAgent::makeKind( false, false, false, false );
    oscagent = new OSCAgent( dk );
    oscagent->addListener( this );
    bounds.init = false;
  
    video_enabled = false;
    sound_enabled = false;
    font_enabled = false;
    
    sound_volume_target = 0;
    sound_volume = 0;
    
    ofDirectory vfolder( "videos" );
    ofDirectory sfolder( "sounds" );
    ofFile tfile( MediaConfig::get()->text_path );
    
    if ( vfolder.isDirectory() ) {
        cout << "video folder found, ";
        vector<ofFile> files = vfolder.getFiles();
        for ( vector<ofFile>::iterator itf = files.begin(); itf != files.end(); ++itf ) {
            videos.push_back( (*itf).getAbsolutePath() );
            cout << "\tvideo: " << (*itf).getAbsolutePath() << endl;
        }
        if ( videos.empty() ) {
            cout << "but is empty";
        } else {
            cout << "contains " << videos.size() << " video(s)";
            video_enabled = true;
        }
        cout << endl;
    }
    
    if ( videos.empty() && sfolder.isDirectory() ) {
        cout << "sound folder found, ";
        vector<ofFile> files = sfolder.getFiles();
        for ( vector<ofFile>::iterator itf = files.begin(); itf != files.end(); ++itf ) {
            cout << "\tsound: " << (*itf).getAbsolutePath() << endl;
            sounds.push_back( (*itf).getAbsolutePath() );
        }
        if ( sounds.empty() ) {
            cout << "but is empty";
        } else {
            cout << "contains " << sounds.size() << " sound(s)";
            sound_enabled = true;
        }
        
        cout << endl;
 
    }
    
    if ( videos.empty() && sounds.empty() && tfile.exists() ) {

        ofBuffer buffer( tfile );
        for ( ofBuffer::Line it = buffer.getLines().begin(), end = buffer.getLines().end(); it != end; ++it) {
            string line = (*it);
            if ( !line.empty() ) {
                sentences.push_back( line );
            }
        }
        
        if ( !sentences.empty() ) {
            // searching for font
            cout << sentences.size() << " txt lines loaded, loading fonts/" << endl;
            ofDirectory ffolder( "fonts" );
            if ( ffolder.isDirectory() ) {
                vector<ofFile> files = ffolder.getFiles();
                if ( !files.empty() ) {
                    cout << "\t" << files[ 0 ].getAbsolutePath() << endl;
                    font_enabled = true;
                    font.load( files[ 0 ].getAbsolutePath(), MediaConfig::get()->font_size, true, true, true );
                }
            }
        }
        
    } else if ( videos.empty() && sounds.empty() && !tfile.exists() ) {
    
        cout << "no text file '" << MediaConfig::get()->text_path << " found, use '-t' to define the path" << endl;
        cerr << "no media loaded!" << endl;
        
    }
    
    current_pos = 0;
    stopped = true;
    
    last_start = 0;
    
}

bool ofApp::lock() {

    if ( MediaConfig::get()->lock_time < 0 ) {
        return true;
    }
    
    bool out = false;
    long now = ofGetElapsedTimeMillis();
    if ( now - last_start > MediaConfig::get()->lock_time ) {
        out = true;
        last_start = now;
    }
    return out;

}

void ofApp::nextMedia() {
    
    if ( !lock() ) return;
    
    if ( videos.empty() && sounds.empty() && sentences.empty() ) {
        return;
    }
    current_pos++;
    if ( !videos.empty() && current_pos >= videos.size() ) {
        current_pos = 0;
    } else if ( videos.empty() && !sounds.empty() && current_pos >= sounds.size() ) {
        current_pos = 0;
    } else if ( videos.empty() && sounds.empty() && !sentences.empty() && current_pos >= sentences.size() ) {
        current_pos = 0;
    }
    loadMedia();
    
}

void ofApp::randomMedia() {
    
    if ( !lock() ) return;
    
    if ( videos.empty() && sounds.empty() && sentences.empty() ) {
        return;
    }
    
    vector< string > * data;
    if ( !videos.empty() ) { data = &videos; }
    else if ( !sounds.empty() ) { data = &sounds; }
    else if ( !sentences.empty() ) { data = &sentences; }
    
    vector<int> available;
    int i = 0;
    for ( vector<string>::iterator itv = data->begin(); itv != data->end(); ++itv ) {
        if ( (*itv).compare( current_path ) != 0 ) {
            available.push_back( i );
        }
        ++i;
    }
    if ( available.empty() ) {
        current_pos = 0;
    } else {
        int p = round( ofRandomuf() * available.size() );
        if ( p >= available.size() ) {
            p = 0;
        }
        current_pos = available[ p ];
    }
    
//    cout << "randomMedia: " << current_pos << endl;

    loadMedia();

}

void ofApp::stopMedia() {
    
    if ( stopped ) return;
    if ( !videos.empty() ) {

#ifdef TARGET_OPENGLES
        if ( vplayer != nullptr ) {
            delete vplayer;
            vplayer = nullptr;
        }
#else       
        vplayer.stop();
#endif
        stopped = true;
    
    } else if ( !sounds.empty() ) {
        
        splayer.stop();
        stopped = true;
    
    } else if ( !sentences.empty() ) {
    
        stopped = true;
    
    }
    
}

void ofApp::loadMedia() {
    
    if ( !videos.empty() ) {

        current_path = videos[ current_pos ];
        cout << "starting video: " << current_path << endl;

#ifdef TARGET_OPENGLES
        
        if ( vplayer != nullptr ) {
            delete vplayer;
            vplayer = nullptr;
        }
        ofxOMXPlayerSettings settings;
        settings.videoPath = current_path;
        settings.useHDMIForAudio = false;
        settings.enableTexture = true;
        settings.enableLooping = false;
        settings.enableAudio = true;
        vplayer = new ofxOMXPlayer();
        vplayer->setup(settings);

#else          
        
        vplayer.load( current_path );
        vplayer.play();
        vplayer.setLoopState( OF_LOOP_NONE );

#endif

        stopped = false;

    } else if ( !sounds.empty() ) {

        current_path = sounds[ current_pos ];
        cout << "starting sound: " << current_path << endl;
        
        splayer.load( current_path );
        splayer.play();
        splayer.setLoop( false );
        splayer.setVolume( sound_volume );
        sound_volume_target = 1;
        stopped = false;

    } else if ( !sentences.empty() ) {
        
        current_path = sentences[ current_pos ];
        font_bounds = font.getStringBoundingBox( current_path, 0, 0 );
        stopped = false;
        
        cout << "starting text: " << current_path << endl;
        
    }
    
}

//--------------------------------------------------------------
void ofApp::update(){

    oscagent->update( ofGetElapsedTimeMillis() );
    
    if ( !stopped ) {
#ifdef TARGET_OPENGLES
        if ( video_enabled && 
            !stopped && 
            vplayer != nullptr &&    
            vplayer->getCurrentFrame() == vplayer->getTotalNumFrames()
        ) {
            delete vplayer;
            vplayer = nullptr;
            stopped = true;
        }
#else
        if ( 
            video_enabled && 
            !stopped && 
            vplayer.isPlaying() && vplayer.getIsMovieDone()
        ) {
            vplayer.stop();
            stopped = true;
        }
        
#endif
    }
    
#ifndef TARGET_OPENGLES

    if ( video_enabled && vplayer.isPlaying() ) {
        vplayer.update();
    }
    
#endif
    
    if ( sound_enabled && sound_volume != sound_volume_target ) {
        if ( sound_volume < sound_volume_target ) {
            sound_volume += 0.1;
            if ( sound_volume > sound_volume_target ) {
                sound_volume = sound_volume_target;
            }
        } else if ( sound_volume > sound_volume_target ) {
            sound_volume -= 0.05;
            if ( sound_volume < sound_volume_target ) {
                sound_volume = sound_volume_target;
            }
        }
        splayer.setVolume( sound_volume );
    }

    uint64_t now = ofGetElapsedTimeMillis();
    vector< string > zombies;
    for ( map< string, timedPoints >::iterator itp = points.begin(); itp != points.end(); ++itp ) {
        if ( now - itp->second.lasttime > Config::get()->max_delay_millis ) {
            zombies.push_back( itp->first );
        }
    }
    for ( vector<string>::iterator itz = zombies.begin(); itz != zombies.end(); ++itz ) {
        points.erase( (*itz) );
    }
    
    if ( 
        MediaConfig::get()->lock_time > 0 &&
        !MediaConfig::get()->manual && 
        points.empty() && 
        !stopped 
    ) {
        stopMedia();
    }
    
    for ( map< string, Request >::iterator itr = requests.begin(); itr != requests.end(); ++itr ) {
        if ( itr->second.nexttime <= ofGetElapsedTimeMillis() ) {
            oscagent->request( itr->first, itr->second.req.i );
            requests[ itr->first ].nexttime = ofGetElapsedTimeMillis() + 5000;
        }
    }
    
}

void ofApp::process( ofxOscMessage & msg ) {
    
    if ( msg.getAddress().compare( OSC_ADDRESS_DATA ) == 0 ) {

        DataKind dk( msg.getArgAsInt( 0 ) );
        
        if ( dk[ 0 ] ) {
        
            string sender_name = msg.getArgAsString( 1 );
            int tagid = msg.getArgAsInt( 2 );
            int event = msg.getArgAsInt( 3 );

            stringstream ss;
//            ss << msg.getRemoteIp() << "-t-" << tagid;
            string utag = ss.str();

            if ( event == TAG_DISAPPEARED && points.find( utag ) != points.end() ) {
            
                points.erase( utag );
            
            } else {
            
                if ( points.find( utag ) == points.end() ) {
                    randomMedia();
                }
                points[ utag ].pos.set( msg.getArgAsFloat( 4 ), msg.getArgAsFloat( 5 ) );
                points[ utag ].lasttime = ofGetElapsedTimeMillis();
            
            }
        
        } else {
        
            cerr << "can't handle this type of data message" << endl;
        
        }
        
    }

}

void ofApp::newAgent( string unique, DataSender & sender ) {

    if ( !sender.kind[ 0 ] ) {
        return;
    }
    
    if ( 
        Config::get()->names_filter.empty() || 
        Config::get()->names_filter.find( sender.name ) != std::string::npos ) {

//        DataKind dk = OSCAgent::makeKind( false, false, false, true );
//        DataKind dk = OSCAgent::makeKind( false, true, true, true );
//        DataKind dk = OSCAgent::makeKind( true, false, false, false );
//        oscagent->request( unique, dk.i );
//        cout << "sending request to " << unique << ", " << int( dk.i ) << endl;
        requests[ unique ].nexttime = ofGetElapsedTimeMillis();
        requests[ unique ].req = OSCAgent::makeKind( true, false, false, false );
    
    } else {
        
        cout << "NOT sending request to " << unique << " called " << sender.name <<  " >> names filter: " << Config::get()->names_filter << endl;
        
    }
    
}

void ofApp::deletedAgent( string unique ) {

}    

//--------------------------------------------------------------
void ofApp::draw(){

    if ( !stopped ) {
        ofBackground( 
            MediaConfig::get()->bg_red,
            MediaConfig::get()->bg_green,
            MediaConfig::get()->bg_blue
        );
    } else {
        ofBackground( 
            MediaConfig::get()->idle_red,
            MediaConfig::get()->idle_green,
            MediaConfig::get()->idle_blue
        );
    }
    
    
    ofPushMatrix();
    ofTranslate( ofGetWidth() * 0.5, ofGetHeight() * 0.5, 0 );
    
    float sw = ofGetWidth();
    float sh = ofGetHeight();
 
#ifdef TARGET_OPENGLES

    if ( video_enabled && vplayer != nullptr && !stopped ) {
        float vw = vplayer->getWidth();
        float vh = vplayer->getHeight();

#else
        
    if ( video_enabled && vplayer.isPlaying() ) {
        float vw = vplayer.getWidth();
        float vh = vplayer.getHeight();
        
#endif
        
        float vidr = vw / vh;
        float screenr = sw / sh;
        
        if ( vidr <= screenr ) {
            vh = sh;
            vw = vh * vidr;
        } else {
            vw = sw;
            vh = vw / vidr;
        }
        
        ofSetColor( 255,255,255 );
        
#ifdef TARGET_OPENGLES
        vplayer->draw( -vw * 0.5, -vh * 0.5, vw, vh );
#else
        vplayer.draw( -vw * 0.5, -vh * 0.5, vw, vh );
#endif

    }
    
    if ( font_enabled && !stopped) {
        ofSetColor( 
            MediaConfig::get()->font_red,
            MediaConfig::get()->font_green,
            MediaConfig::get()->font_blue,
            MediaConfig::get()->font_alpha
        );
        float scale;
        if ( font_bounds.width != ofGetWidth() - 150 ) {
            scale = ( ofGetWidth() - 150 ) / font_bounds.width;
        }
        ofPushMatrix();
        ofScale( scale, scale, scale );
        font.drawString( current_path, -font_bounds.width * 0.5, font_bounds.height * 0.5 );
        ofPopMatrix();
    }
    
    ofPopMatrix();
    
    
    if ( MediaConfig::get()->trace_alpha > 0 ) {
        ofSetColor( 
            MediaConfig::get()->trace_red,
            MediaConfig::get()->trace_green,
            MediaConfig::get()->trace_blue,
            MediaConfig::get()->trace_alpha
        );
        stringstream ss;
        ss << "fps: " << ofGetFrameRate() << ", num of tags: " << points.size();
        ofDrawBitmapString( ss.str(), 10, 25 );
        ofDrawBitmapString( oscagent->toString(), 10, 40 );
    }
    
    // trying to fix omx player color bug:
    ofSetColor( 255,255,255 );
    
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
    
    if ( MediaConfig::get()->manual ) {
        if ( key == 's' ) {
            stopMedia();
        } else {
            randomMedia();
        }
    }
    
//    if ( sthread != nullptr ) {
//        sthread->stop();
//        delete sthread;
//        sthread = nullptr;
//    }
    
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

    if ( MediaConfig::get()->manual ) {
        randomMedia();
    }
    
}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}