#pragma once

#include "ofMain.h"
#include "OSCAgent.h"
#include "MediaConfig.h"
#include "SystemThread.h"

#ifdef TARGET_OPENGLES
#include "ofxOMXPlayer.h"
#endif

struct Boundary {
    ofVec2f min; 
    ofVec2f max; 
    bool init;
};

struct timedPoints { 
   ofVec2f pos;
    uint64_t lasttime;
};

struct Request {
    uint64_t nexttime;
    DataKind req;
};

class ofApp : public ofBaseApp, public OSCagentListener {
public:
    
    void setup();
    void update();
    void draw();

    void keyPressed(int key);
    void keyReleased(int key);
    void mouseMoved(int x, int y);
    void mouseDragged(int x, int y, int button);
    void mousePressed(int x, int y, int button);
    void mouseReleased(int x, int y, int button);
    void mouseEntered(int x, int y);
    void mouseExited(int x, int y);
    void windowResized(int w, int h);
    void dragEvent(ofDragInfo dragInfo);
    void gotMessage(ofMessage msg);
 
    void process( ofxOscMessage & msg );
    void newAgent( string unique, DataSender & sender );
    void deletedAgent( string unique );
    
private:
    
    OSCAgent * oscagent;
    
    bool video_enabled;
    bool sound_enabled;
    vector< string > videos;
    vector< string > sounds;
    vector< string > sentences;
    string current_path;
    int current_pos;
    bool stopped;
    
    ofTrueTypeFont font;
    ofRectangle font_bounds;
    bool font_enabled;
    
    
#ifdef TARGET_OPENGLES
    ofxOMXPlayer * vplayer;
    SystemThread * sthread;
    string start_cmd;
    string stop_cmd;
#else
    ofVideoPlayer vplayer;
#endif
    
    ofSoundPlayer splayer;
    
    float sound_volume_target;
    float sound_volume;
    
    map< string, timedPoints > points;
    Boundary bounds;
    
    map< string, int > requests;
    
    void loadMedia();
    void nextMedia();
    void randomMedia();
    void stopMedia();
    
    bool lock();
    
    long last_start;
    
};
