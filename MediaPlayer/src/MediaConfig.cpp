/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   MediaConfig.cpp
 * Author: frankiezafe
 * 
 * Created on June 18, 2016, 6:09 PM
 */

#include "MediaConfig.h"

static MediaConfig * _instance = nullptr;

MediaConfig * MediaConfig::get() {

    if ( _instance == nullptr ) {
        _instance = new MediaConfig();
    }
    
    return _instance;

}

MediaConfig::MediaConfig() {

    idle_red = 255;
    idle_green = 0;
    idle_blue = 0;
    
    bg_red = 255;
    bg_green = 255;
    bg_blue = 0;
    
    font_red = 0;
    font_green = 0;
    font_blue = 255;
    font_alpha = 255;
    
    trace_red = 255;
    trace_green = 255;
    trace_blue = 0;
    trace_alpha = 255;

    font_size = 160;
    
    text_path = "text.txt";
    
    manual = false;
    
    lock_time = -1;
    
}

MediaConfig::~MediaConfig() {}
