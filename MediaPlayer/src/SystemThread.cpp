/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   SystemThread.cpp
 * Author: frankiezafe
 * 
 * Created on June 21, 2016, 4:07 PM
 */

#include "SystemThread.h"

SystemThread::SystemThread( ) {
}

SystemThread::~SystemThread( ) {
}

void SystemThread::start( ) {
    startThread(true);
}

void SystemThread::stop() {
    system( stop_cmd.c_str() );
    stopThread();
}

void SystemThread::threadedFunction() {

    system( start_cmd.c_str() );
    cout << "SystemThread called and finished" << endl;
    
}