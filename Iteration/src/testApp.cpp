#include "testApp.h"

void testApp::setup( ) {

    ofSetLogLevel( OF_LOG_FATAL_ERROR );
    
    width = Config::get()->cam_width;
    height = Config::get()->cam_height;

#ifdef CAMERA_CONNECTED
    vidGrabber.initGrabber( width, height );
#else
    vidPlayer.loadMovie( "marker.mov" );
    vidPlayer.play( );
    vidPlayer.setLoopState( OF_LOOP_NORMAL );
#endif

    colorImage.allocate( width, height );
    grayImage.allocate( width, height );
//    grayThres.allocate( width, height );

//    displayImage.loadImage( "of.jpg" );
//    int displayImageHalfWidth = displayImage.getWidth( ) / 2;
//    int displayImageHalfHeight = displayImage.getHeight( ) / 2;
//    displayImageCorners.push_back( ofPoint( 0, 0 ) );
//    displayImageCorners.push_back( ofPoint( displayImage.getWidth( ), 0 ) );
//    displayImageCorners.push_back( ofPoint( displayImage.getWidth( ), displayImage.getHeight( ) ) );
//    displayImageCorners.push_back( ofPoint( 0, displayImage.getHeight( ) ) );
    artk.setup( width, height );
    artk.setThreshold( Config::get()->threshold );

    ofBackground( 127, 127, 127 );

    if ( Config::get()->enable_osc ) {
        DataKind k = OSCAgent::makeKind( true, true, true, true );
        oscagent = new OSCAgent( k );
    } else {
        oscagent = nullptr;
    }

}

//--------------------------------------------------------------

void testApp::update( ) {

    if ( oscagent != nullptr ) {
        oscagent->update( ofGetElapsedTimeMillis() );
    }
    
#ifdef CAMERA_CONNECTED
    vidGrabber.update( );
    bool bNewFrame = vidGrabber.isFrameNew( );
#else
    vidPlayer.update( );
    bool bNewFrame = vidPlayer.isFrameNew( );
#endif

    if ( bNewFrame ) {
#ifdef CAMERA_CONNECTED
        colorImage.setFromPixels( vidGrabber.getPixels( ), width, height );
#else
        colorImage.setFromPixels( vidPlayer.getPixels( ), width, height );
#endif
        grayImage = colorImage;
//        grayThres = grayImage;
//        grayThres.threshold( threshold );
        artk.update( grayImage.getPixels( ) );
        int numDetected = artk.getNumDetectedMarkers( );

        
        if ( Config::get()->enable_osc ) {
            
            map< tagid, tagcount > current_tags;
    
            for ( int i = 0; i < numDetected; i++ ) {
            
                DataSet data;

                ofMatrix4x4 amat = artk.getMatrix( i );
                // invert matrix
                for ( int r = 0; r < 4; ++r ) {
                for ( int c = 0; c < 4; ++c ) {
                    int i = c + r * 4;
                    int j = c * 4 + r;
                    data.mat.getPtr()[ i ] = amat.getPtr()[ j ];
                }
                }
                
                data.tagid = artk.getMarkerID( i );
                data.xyz = data.mat.getTranslation();
                data.zrotation = data.mat.getRotate().getEuler().z;
               
                current_tags[ data.tagid ] ++;
                if ( 
                    previous_tags.find( data.tagid ) == previous_tags.end() ||
                    previous_tags[ data.tagid ] < current_tags[ data.tagid ]
                    ) {
                    data.event = TAG_APPEAR;
                } else {
                    data.event = TAG_PRESENT;
                }
                
                oscagent->send( data );
                
            }
            
            // comparison between previous and current tags
            for ( map< tagid, tagcount >::iterator itt = previous_tags.begin(); itt != previous_tags.end(); ++itt ) {
                if( current_tags.find( itt->first ) == current_tags.end() ) {
                    for ( int i = 0; i < itt->second; ++i ) {
                        DataSet data;
                        data.tagid = itt->first;
                        data.event = TAG_DISAPPEARED;
                        oscagent->send( data );
                    }
                } else if ( current_tags[ itt->first ] < itt->second ) {
                    int delta = itt->second - current_tags[ itt->first ];
                    for ( int i = 0; i < delta; ++i ) {
                        DataSet data;
                        data.tagid = itt->first;
                        data.event = TAG_DISAPPEARED;
                        oscagent->send( data );
                    }
                }
            }
            
            previous_tags.clear();
            previous_tags.insert( current_tags.begin(), current_tags.end() );
        
        }
        
        
        
    }
    
}

//--------------------------------------------------------------

void testApp::draw( ) {

#ifdef NO_WIDOW
#else
    ofBackground( 255, 0, 0 );
    ofSetHexColor( 0xffffff );
    vidGrabber.draw( 0, 0 );
    grayImage.draw( Config::get()->cam_width,0 );
//    grayThres.draw( 0,Config::get()->cam_height );
    artk.draw( 0, 0 );
    artk.applyProjectionMatrix( );
    int numDetected = artk.getNumDetectedMarkers( );
    ofEnableAlphaBlending( );
    for ( int i = 0; i < numDetected; i++ ) {
        artk.applyModelMatrix( i );
        ofNoFill( );
        ofSetLineWidth( 5 );
        ofSetHexColor( 0xffffff );
        ofNoFill( );
        ofEnableSmoothing( );
        ofSetColor( 255, 255, 0, 50 );
        for ( int i = 0; i < 10; i++ ) {
            ofRect( -25, -25, 50, 50 );
            ofTranslate( 0, 0, i * 1 );
        }
    }


#endif
    
//    // Main image
//    ofSetHexColor( 0xffffff );
//    grayImage.draw( 0,0 );
//    ofSetHexColor( 0x666666 );
//    ofDrawBitmapString( ofToString( artk.getNumDetectedMarkers( ) ) + " marker(s) found", 10, 20 );
//
//    // Threshold image
//    ofSetHexColor( 0xffffff );
//    grayThres.draw( 640, 0 );
//    ofSetHexColor( 0x666666 );
//    ofDrawBitmapString( "Threshold: " + ofToString( threshold ), 650, 20 );
//    ofDrawBitmapString( "Use the Up/Down keys to adjust the threshold", 650, 40 );
//
//    vidGrabber.draw( 0, 0 );
//
//    // ARTK draw
//    // An easy was to see what is going on
//    // Draws the marker location and id number
//    artk.draw( 640, 0 );
//
//    // ARTK 2D stuff
//    // See if marker ID '0' was detected
//    // and draw blue corners on that marker only
//    int myIndex = artk.getMarkerIndex( 0 );
//    if ( myIndex >= 0 ) {
//        // Get the corners
//        vector<ofPoint> corners;
//        artk.getDetectedMarkerBorderCorners( myIndex, corners );
//        // Can also get the center like this:
//        // ofPoint center = artk.getDetectedMarkerCenter(myIndex);
//        ofSetHexColor( 0x0000ff );
//        for ( int i = 0; i < corners.size( ); i++ ) {
//            ofCircle( corners[i].x, corners[i].y, 10 );
//        }
//        // Homography
//        // Here we feed in the corners of an image and get back a homography matrix
//        ofMatrix4x4 homo = artk.getHomography( myIndex, displayImageCorners );
//        // We apply the matrix and then can draw the image distorted on to the marker
//        ofPushMatrix( );
//        glMultMatrixf( homo.getPtr( ) );
//        ofSetHexColor( 0xffffff );
//        displayImage.draw( 0, 0 );
//        ofPopMatrix( );
//
//    }
//
//
//    artk.applyProjectionMatrix( );
//    int numDetected = artk.getNumDetectedMarkers( );
//    ofEnableAlphaBlending( );
//    for ( int i = 0; i < numDetected; i++ ) {
//        artk.applyModelMatrix( i );
//        ofNoFill( );
//        ofSetLineWidth( 5 );
//        ofSetHexColor( 0xffffff );
//        ofNoFill( );
//        ofEnableSmoothing( );
//        ofSetColor( 255, 255, 0, 50 );
//        for ( int i = 0; i < 10; i++ ) {
//            ofRect( -25, -25, 50, 50 );
//            ofTranslate( 0, 0, i * 1 );
//        }
//    }

}

//--------------------------------------------------------------

void testApp::keyPressed( int key ) {
    if ( key == OF_KEY_UP ) {
        artk.setThreshold( ++Config::get()->threshold );

    } else if ( key == OF_KEY_DOWN ) {
        artk.setThreshold( --Config::get()->threshold );
    }
#ifdef CAMERA_CONNECTED
    if ( key == 's' ) {
        vidGrabber.videoSettings( );
    }
#endif
}