#!/bin/bash

# CONFIGURATION
password="alarm"
# duration in seconds
ka_sleep=1
# duration in minutes
ka_max_runningtime=120

# wait 10 seconds
sleep 10

cd /home/alarm/of_v0.9.3/apps/iterations2016
pwd

NAME=`hostname`
echo "--- Starting $NAME ---"

echo "setting tvservice on for vga support"
/opt/vc/bin/tvservice -e "DMT 16 HDMI"

#echo "Starting the tag tracker"
#Iteration/bin/./Iteration -verbose -n $NAME -s 320x240 &

#echo "Starting the map"
#MapDistortion/bin/./MapDistortion -n $NAME &

#echo "Starting web"
#cd web
#/usr/bin/node app.js -n $NAME &
#/usr/bin/xinit ../scripts/web.sh &

#echo "Tagging and playing videos at the same time"
#./Iteration/bin/Iteration -n ${NAME}-camera -s 320x240 &
#sleep 5
#./MediaPlayer/bin/MediaPlayer -n ${NAME}-video -p 24000 &

# WORKING VARS
td=$(date +%Y%m%d)
ths=$(date +%H)
tms=$(date +%M)
ka_previoustime=$(( $td * 24 * 60 ))
ka_previoustime=$(( $ka_previoustime + $ths * 60 ))
ka_previoustime=$(( $ka_previoustime + $tms ))
ka_runningtime=0

while :
do
	ka_d=$(date +%Y%m%d)
	ka_hs=$(date +%H)
	ka_ms=$(date +%M)
	ka_time=$(( $ka_d * 24 * 60 ))
	ka_time=$(( $ka_time + $ka_hs * 60 ))
	ka_time=$(( $ka_time + $ka_ms ))
	
	ka_deltams=$(( $ka_time - $ka_previoustime ))
	ka_runningtime=$(( $ka_runningtime + $ka_deltams ))
	#echo "h: $ka_hs ,m: $ka_ms => $ka_time <> $ka_previoustime, running time: $ka_runningtime"
	
	if [ $ka_runningtime -gt $ka_max_runningtime ]; then
		echo "max running time reached, let's reboot!"
		echo $password | sudo -S reboot
	fi
	ka_previoustime=$ka_time
	sleep $ka_sleep
	
done
