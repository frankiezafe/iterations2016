#pragma once

#include "ofMain.h"
#include "ofxCv.h"
#include "OSCAgent.h"
#include "MotionConfig.h"
#include "MotionAgent.h"

class motionApp : public ofBaseApp, public OSCagentListener {
public:

    void setup();
    void update();
    void draw();

    // OSCagentListener
    void process( ofxOscMessage & msg );
    void newAgent( string unique, DataSender & sender );
    void deletedAgent( string unique );
    
protected:
    ofVideoGrabber cam;
    ofxCv::RunningBackground background;
    ofImage thresholded;
    
    float p;
    int pxnum;
    float thresh;
    
    MotionAgent * magent;
    
}; 
