/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   MotionConfig.h
 * Author: frankiezafe
 *
 * Created on June 22, 2016, 2:17 PM
 */

#ifndef MOTIONCONFIG_H
#define MOTIONCONFIG_H

class MotionConfig {
public:
    
    static MotionConfig * get();
    
    int learning_time;
    
private:
    MotionConfig();
    virtual ~MotionConfig();

};

#endif /* MOTIONCONFIG_H */

