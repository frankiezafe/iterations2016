/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   MotionConfig.cpp
 * Author: frankiezafe
 * 
 * Created on June 22, 2016, 2:17 PM
 */

#include "MotionConfig.h"

static MotionConfig * _instance = nullptr;

MotionConfig * MotionConfig::get() {

    if ( _instance == nullptr ) {
        _instance = new MotionConfig();
    }
    
    return _instance;

}


MotionConfig::MotionConfig( ) {
    learning_time = 10;
}

MotionConfig::~MotionConfig( ) {
}

