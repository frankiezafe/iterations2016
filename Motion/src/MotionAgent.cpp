/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   MotionAgent.cpp
 * Author: frankiezafe
 * 
 * Created on June 22, 2016, 2:26 PM
 */

#include "MotionAgent.h"

void MotionAgent::send( float presence ) {

    ofxOscMessage msg;
    msg.setAddress( OSC_ADDRESS_DATA );
    msg.addIntArg( kind.i );
    msg.addStringArg( Config::get()->name );
    msg.addFloatArg( presence );
    
    for ( map< string, DataSender * >::iterator its = senders.begin(); its != senders.end(); ++its ) {
        if ( its->second->request.i == 0 || !its->second->request[4] ) {
			// can't deliver any information to this agent...
            continue;
        }
        its->second->out.sendMessage( msg, false );
    }
    
}
