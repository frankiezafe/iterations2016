Array.prototype.find = function (predicate, thisValue) {
        var arr = Object(this);
        if (typeof predicate !== 'function') {
            throw new TypeError();
        }
        for(var i=0; i < arr.length; i++) {
            if (i in arr) {  // skip holes
                var elem = arr[i];
                if (predicate.call(thisValue, elem, i, arr)) {
                    return elem;  // (1)
                }
            }
        }
        return undefined;  // (2)
    }

Array.prototype.findIndex = function (predicate, thisValue) {
        var arr = Object(this);
        if (typeof predicate !== 'function') {
            throw new TypeError();
        }
        for(var i=0; i < arr.length; i++) {
            if (i in arr) {  // skip holes
                var elem = arr[i];
                if (predicate.call(thisValue, elem, i, arr)) {
                    return i;  // (1)
                }
            }
        }
        return -1;  // (2)
    }

// SETUP
var socket = io.connect();

var nodes = [];

// news
socket.on('news', function (data) {
	console.log(data);
});

var deleteAfter20 = function( el, i ){
	el.alive = setTimeout(function(){
		el.remove();
		nodes.splice(i, 1);
	}, 20000);
}

var resetAlive20 = function( el, i ){
	clearTimeout(el.alive);
	deleteAfter20( el, i );
}

// news
socket.on('♥', function (data) {
	var tmpIP = data[0] + ':' + data[1]; // IP + Port
	// console.log(tmpIP);
	var index = -1;
	if (nodes.length > 0 ) index = nodes.findIndex( function(node){ return node[0].id == tmpIP } )
	if ( index >= 0 ) {
		// console.log(nodes[index][0].id + " is already alive");
		resetAlive20( nodes[index], index );
	} else {
		console.log("Pushing " + data);
		var newLi = $("<li id='" + data[0] + ":" + data[1] + "'><h3>" + data[3] + "</h3><ul id='tags'></ul></li>");
		newLi.stupidName = data[3];
		deleteAfter20( newLi, index );
		nodes.push(newLi);
		$("#nodes").append(newLi);
	}
});

socket.on('d', function(data) {
		// console.log("Where is " + data[0])
  	var node = nodes.find(function(node){ return node.stupidName == data[0] } );
		if (node) {
			var tags = $.makeArray(node.find('#tags li'));
			var tag = tags.find(function(tag){ return tag.id == data[1] } );
			if ( tag ) {
				$(tag).text( data[4].toFixed(2) + ", " + data[5].toFixed(2) )
				if( data[2] == 2){
					$(tag).remove()
				}
			} else {
				node.find("ul").append("<li id='" + data[1].toFixed(2) + "'>" + data[4].toFixed(2) + ", " + data[5] +"</li>")
			}
		}
})
