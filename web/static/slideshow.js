Array.prototype.find = function (predicate, thisValue) {
        var arr = Object(this);
        if (typeof predicate !== 'function') {
            throw new TypeError();
        }
        for(var i=0; i < arr.length; i++) {
            if (i in arr) {  // skip holes
                var elem = arr[i];
                if (predicate.call(thisValue, elem, i, arr)) {
                    return elem;  // (1)
                }
            }
        }
        return undefined;  // (2)
    }

Array.prototype.findIndex = function (predicate, thisValue) {
        var arr = Object(this);
        if (typeof predicate !== 'function') {
            throw new TypeError();
        }
        for(var i=0; i < arr.length; i++) {
            if (i in arr) {  // skip holes
                var elem = arr[i];
                if (predicate.call(thisValue, elem, i, arr)) {
                    return i;  // (1)
                }
            }
        }
        return -1;  // (2)
    }

// SETUP
var socket = io.connect();
var nodes = [];

var deleteAfter20 = function( el, i ){
	el.alive = setTimeout(function(){
		el.remove();
		nodes.splice(i, 1);
	}, 20000);
}

var resetAlive20 = function( el, i ){
	clearTimeout(el.alive);
	deleteAfter20( el, i );
}

// news
socket.on('news', function (data) {
	console.log(data);
});

// heartbeat
socket.on('♥', function (data) {
	var tmpIP = data[0];
	//console.log(tmpIP);
});

// data from tag
socket.on('d', function(data) {
	// console.log("Where is " + data[0])
  if ( data[3] == 0 ){
    // console.log(data[1] + " -> tag: " + data[2] + " first apparition")
    // trigger slides show
    triggerNewSlide();
  } else if ( data[3] == 1 ){
    // console.log(data[1] + " -> tag: " + data[2] + " presence")
  } else if ( data[3] == 2 ) {
    // console.log(data[1] + " -> tag: " + data[2] + " disparition")
  }
});

$(document).ready(function(){
  var scr = $(window);
  var height = scr.height();
  var width = scr.width();
  console.log(height, width);
  $('#slideshow').css({
    height: height + "px",
    width: width +"px"
  });
});
var images = $('#slideshow img');
console.log(images);
var startImageId = 0;

var hideAllImages = function(){
  images.each(function(){
    $(this).hide()
  });
}

hideAllImages();
// slideshow
var triggerNewSlide = function(){
  var lastImageId = images.length - 1;
  /*
  if (startImageId == 0){
    $(images[ lastImageId ]).hide();
  } else {
    $(images[ startImageId ]).hide();
  }
  */
  hideAllImages();
  startImageId++;
  if (startImageId > lastImageId) startImageId = 0;
  $(images[startImageId]).show();
  console.log($(images[startImageId]));
};
