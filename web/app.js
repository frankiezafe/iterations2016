broadcast_ip = "10.0.0.255";
broadcast_port = 20000;
data_port = 23100;

var fs = require('fs');
var osc = require("osc");
var express = require('express');
var ip = require('ip');
var argv = require('minimist')(process.argv.slice(2));

var app = express();
// Set the default template engine to "jade"
app.set('views', __dirname + '/views');
app.set('view engine', 'pug');
// Make whatever is in the /static folder available
app.use(express.static( __dirname + '/static'));

var http = require('http').Server(app);
var io = require('socket.io').listen(http);

var myIP = ip.address();

// Check if a name was provided
if ( !argv.n ){
  console.error("/!\\ No Name provided");
  console.log("Usage: node app.js -n <NAME>");
  process.exit();
}
var myName = argv.n;

// Create an osc.js UDP Port listening on port 20000
var broadcast = new osc.UDPPort({
    localAddress: "0.0.0.0",
    localPort: 20000,    // Heartbeat
    broadcast: true
});

// Send heartbeat to the client
broadcast.on("message", function (oscMsg) {
    //console.log(" ♥ ", oscMsg.args);
    io.emit('♥', oscMsg.args);
    sendDataRequest( oscMsg.args[0], 23000, 1)
});

// Create data connection
var data = new osc.UDPPort({
  localAddress: "0.0.0.0",
  localPort: data_port
});

// Send data to the client
data.on("message", function (oscMsg) {
  //console.log(" data ", oscMsg);
  if (oscMsg.address == '/d'){
    io.emit('d', oscMsg.args);
  }
});

// Send dada request to a node that has tags
var sendDataRequest = function(ip, port, dataKind){
  data.send({
    address: "/r",
    args: [
      myIP,                         // My IP
      { type: "i", value: port },  // Port
      { type: "i", value: 0 },      // Output Datakind
      { type: "i", value: dataKind },      // Input Datakind
    ]
  }, ip, 23000 )
}


// Sending a heartbeat message
var sendHeartbeat = function(){
  broadcast.send({
      address: "/a",
      args: [
        myIP,
        { type: "i", value: 21000 },  // Port
        { type: "i", value: 0 },  // Datakind
        myName  // Name
      ]
  },broadcast_ip, broadcast_port);
}

io.on('connection', function (socket) {
  socket.emit('news', { hello: 'world' });
});

var sendDataRequesttoAll = function(){
  var ip = [
    "10.0.0.11",
    "10.0.0.12",
    "10.0.0.13",
    "10.0.0.14",
    "10.0.0.15",
    "10.0.0.16",
    "10.0.0.17",
    "10.0.0.18",
    "10.0.0.19",
    "10.0.0.20",
    "10.0.0.21",
    "10.0.0.22",
    "10.0.0.23",
    "10.0.0.24",
    "10.0.0.25"
  ];
  ip.forEach(function(ip, index){
    sendDataRequest(ip, data_port, 15);
  });
}

setInterval(sendDataRequesttoAll, 10000);

var imageFolder = __dirname + '/images';
var slides = [];
slides = fs.readdirSync( imageFolder );
slides.sort();
app.use('/slides', express.static( imageFolder ));
console.log(slides)

/** MAIN **********************************************************************/
// Open the socket.
//broadcast.open();
data.open();
// Start heartbeat
//setInterval(sendHeartbeat, 5000);

sendDataRequesttoAll();

http.listen(3000, function(){
  console.log('listening on *:3000');
  // console.log('__dirname: ' + __dirname);
  console.log('identified as ' + myName);
});

app.get('/', function(req, res){
  res.render( 'index', {} );
});

app.get('/shipmap', function(req, res){
  res.render( 'shipmap', {} );
});

app.get('/slideshow', function(req, res){
  res.render( 'slideshow', {slides: slides} );
})
