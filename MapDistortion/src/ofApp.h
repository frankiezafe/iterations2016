#pragma once

#include "ofMain.h"
#include "OSCAgent.h"
#include "Grid.h"
#include "MapConfig.h"

enum BrushType {
    BRUSH_NONE =            0,
    BRUSH_PUSH =            1,
    BRUSH_PUSH_FORCE =      2,
    BRUSH_PULL =            3,
    BRUSH_PINCH =           4,
    BRUSH_INFLATE =         5,
    BRUSH_COUNT =           6
};

struct Boundary {
    ofVec2f min;
    ofVec2f max;
    bool init;
};

struct BrushPoint {
    ofVec2f pos;
    BrushType type;
    float radius;
    float force;
    bool limit;
    long lastseen;
};

struct Request {
    uint64_t nexttime;
    DataKind req;
};

class ofApp : public ofBaseApp, public OSCagentListener {
public:
    
    void setup();
    void update();
    void draw();

    void keyPressed(int key);
    void keyReleased(int key);
    void mouseMoved(int x, int y);
    void mouseDragged(int x, int y, int button);
    void mousePressed(int x, int y, int button);
    void mouseReleased(int x, int y, int button);
    void mouseEntered(int x, int y);
    void mouseExited(int x, int y);
    void windowResized(int w, int h);
    void dragEvent(ofDragInfo dragInfo);
    void gotMessage(ofMessage msg);
 
    void process( ofxOscMessage & msg );
    void newAgent( string unique, DataSender & sender );
    void deletedAgent( string unique );
    
private:
    
    OSCAgent * oscagent;
    
    map< string, BrushPoint > bpoints;
    ofVec2f barycenter;
    Boundary bounds;
    
    map< string, Request > requests;
    vector< string > maps;
    
    Grid mapgrid;
    
    BrushType btype;
    
};
