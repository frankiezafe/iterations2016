/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   MediaConfig.h
 * Author: frankiezafe
 *
 * Created on June 18, 2016, 6:09 PM
 */

#ifndef MEDIACONFIG_H
#define MEDIACONFIG_H

#include <string>
#include <string>
#include <sstream>
#include <vector>


class MapConfig {

public:

    static std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems) {
        std::stringstream ss(s);
        std::string item;
        while (getline(ss, item, delim)) {
            elems.push_back(item);
        }
        return elems;
    }


    static std::vector<std::string> split(const std::string &s, char delim) {
        std::vector<std::string> elems;
        split(s, delim, elems);
        return elems;
    }

    static MapConfig * get();
    
    float up_red;
    float up_green;
    float up_blue;
    float down_red;
    float down_green;
    float down_blue;
    int grid_resolution;
    int position_max;
    float z_range;
    float smoothing;
    
    float wave_ampl_min;
    float wave_ampl_max;
    float wave_freq_min;
    float wave_freq_max;
    
    float radius_min;
    float radius_max;
    float force_min;
    float force_max;
    
    float chance_for_map;
    float rotation_mult;
    
    bool display_debug;
    
private:
    
    MapConfig();
    virtual ~MapConfig();

};

#endif /* MEDIACONFIG_H */

