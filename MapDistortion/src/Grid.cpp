/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Grid.cpp
 * Author: frankiezafe
 * 
 * Created on June 15, 2016, 8:24 PM
 */

#include "Grid.h"
#include "MapConfig.h"

Grid::Grid( ) {
    
    base_points = nullptr;
    smoothing = MapConfig::get()->smoothing;
    impath = "";
    
    color_z = MapConfig::get()->z_range;
    defaultcolor.set( 1,1,1 );
    upcolor.set( MapConfig::get()->up_red, MapConfig::get()->up_green, MapConfig::get()->up_blue );
    downcolor.set( MapConfig::get()->down_red, MapConfig::get()->down_green, MapConfig::get()->down_blue );
    upcolor /= 255;
    downcolor /= 255;
    
    wave_amplitude = 0;
    wave_frequency = 0;
    wave_phase = 0;
    
}

Grid::~Grid( ) {
}

void Grid::loadMap( string path ) {

    if ( impath.compare( path ) == 0 ) {
        return;
    }
    
    impath = path;
    map.load( path );
    map.setUseTexture( true );

}

void Grid::load( string path, int columns, int rows ) {
    
    loadMap( path );
    
    this->cols = columns;
    this->rows = rows;
    
    ptnum = ( columns + 1 ) * ( rows + 1 );
    base_points = new GridPoint[ ptnum ];
    current = new ofVec3f[ ptnum ];
    fnum = columns * rows * 2;

    float cellwidth = ( map.getWidth() - 1 ) * 1.f / columns;
    float cellheight = ( map.getHeight() - 1 ) * 1.f / rows;
    
    float offsetx = -map.getWidth() * 0.5;
    float offsety = -map.getHeight() * 0.5;
    
    int i = 0;
    int ip = 0;
    for ( float y = 0; y <= map.getHeight(); y += cellheight ) {
    for ( float x = 0; x <= map.getWidth(); x += cellwidth ) {
        
            i = (int) ( x + floor( y ) * map.getWidth() );
            float px = x;
            float py = y;
            base_points[ ip ].set(
                            offsetx + px, offsety + py, 0,
                            px, py, 0 );
            
            current[ ip ] = (ofVec3f &) base_points[ ip ];
            
            mesh.addVertex( (ofVec3f &) base_points[ ip ] );
#ifdef TARGET_OPENGLES
            mesh.addTexCoord( ofVec2f( px / map.getWidth(), py / map.getHeight() ) );
#else
            mesh.addTexCoord( ofVec2f( px, py ) );
#endif
            mesh.addColor( ofFloatColor( 1,1,1,1 ) );
            ip++;
    }
    }
    
    int f = 0;
    int pcols = ( columns + 1 );
    for( int r = 0; r < rows; ++r ) {
    for( int c = 0; c < columns; ++c ) {
            int tli = c + r * pcols;
            int tri = ( c + 1 ) + r * pcols;
            int bri = ( c + 1 ) + ( r + 1 ) * pcols;
            int bli = c + ( r + 1 ) * pcols;
            mesh.addTriangle( tli, tri, bli );
            f++;
            mesh.addTriangle( tri, bri, bli );
            f++;
    }
    }
    
}

void Grid::update() {
    
    wave_phase += wave_frequency;
    
    ofVec3f c;
    ofVec3f * vertices = mesh.getVerticesPointer();
    ofFloatColor * colors =  mesh.getColorsPointer();
    for ( int i = 0; i < ptnum; ++i ) {
        
        current[ i ] += ( ((ofVec3f & ) base_points[ i ] ) - current[ i ] ) * smoothing;
        
        int y = i % cols;
        int x = i - y * rows;
        float s = sin( wave_phase + ( wave_frequency * y * x ) ) * wave_amplitude;
        
        vertices[ i ] = current[ i ]; 
//        vertices[ i ].x += s * 2;
//        vertices[ i ].y += s * 2;
        vertices[ i ].z += s;
        
        if ( vertices[ i ].z == 0 ) { continue; }
        
        float z = vertices[ i ].z / color_z;
        if ( z < 0 ) {
            z *= -1;
            if ( z > 1 ) z = 1;
            lerp( defaultcolor, downcolor, z, c );
        } else {
            if ( z > 1 ) z = 1;
            lerp( defaultcolor, upcolor, z, c );        
        }
        
        colors[ i ].r = c[ 0 ];
        colors[ i ].g = c[ 1 ];
        colors[ i ].b = c[ 2 ];
        
    }
    
}

void Grid::draw() {

    float b;
    
    ofFill();
    ofSetColor( 255,255,255 );
    map.getTextureReference().bind();
    mesh.draw();
    map.getTextureReference().unbind();

}

void Grid::push( float x, float y, float radius, float force, bool limit ) {
    for ( int i = 0; i < ptnum; ++i ) {
        float d = ofDist( x, y, current[ i ].x, current[ i ].y );
        if ( d <= radius ) {
            float local_force = force * ( ( sin( -HALF_PI + ( PI *  ( 1 - ( d / radius ) ) ) ) + 1 ) * 0.5 );
            if ( 
                ( local_force < 0 && current[ i ].z > -color_z && limit ) ||
                ( local_force > 0 && current[ i ].z < color_z && limit ) ||
                !limit
            ) {
                current[ i ].z += color_z * local_force;
            }
        }
    }
}

void Grid::pinch( float x, float y, float radius, float force ) {
    for ( int i = 0; i < ptnum; ++i ) {
        float d = ofDist( x, y, current[ i ].x, current[ i ].y );
        if ( d <= radius ) {
            float local_force = force * ( ( sin( -HALF_PI + ( PI *  ( 1 - ( d / radius ) ) ) ) + 1 ) * 0.5 );
            if ( force > 0 ) {
                current[ i ].x = current[ i ].x * ( 1 - local_force ) + x * local_force;
                current[ i ].y = current[ i ].y * ( 1 - local_force ) + y * local_force;
            } else {
                current[ i ].x += ( x - current[ i ].x ) * local_force;
                current[ i ].y += ( y - current[ i ].y ) * local_force;
//                current[ i ].z *= 1 - local_force;
            }
        }
    }
}
