/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Grid.h
 * Author: frankiezafe
 *
 * Created on June 15, 2016, 8:24 PM
 */

#ifndef GRID_H
#define GRID_H

#include "GridFace.h"

class Grid {
    
public:
    
    static void lerp( const ofVec3f & start, const ofVec3f & stop, float pc, ofVec3f & dst ) {
        float cp = 1 - pc;
        dst.x = start.x * cp + stop.x * pc;
        dst.y = start.y * cp + stop.y * pc;
        dst.z = start.z * cp + stop.z * pc;
    }
    
    Grid();
    virtual ~Grid();

    void load( string path, int columns, int rows );
    void loadMap( string path );
    void update();
    void draw();
    void push( float x, float y, float radius, float force, bool limit = true );
    void pinch( float x, float y, float radius, float force );
    
    inline void setWave( float ampl, float freq ) {
        wave_amplitude = ampl;
        wave_frequency = freq;
    }
    
    // delegates
    inline int getWidth() { return map.getWidth(); }
    inline int getHeight() { return map.getHeight(); }
    
private:

    ofMesh mesh;
    
    int cols;
    int rows;
    int ptnum;
    GridPoint * base_points;
    ofVec3f * current;
    int fnum;
    
    string impath;
    ofImage map;
    
    float color_z;
    ofVec3f defaultcolor;
    ofVec3f upcolor;
    ofVec3f downcolor;
    
    float smoothing;
    
    float wave_amplitude;
    float wave_frequency;
    float wave_phase;
    
};

#endif /* GRID_H */

