/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   GridPoint.h
 * Author: frankiezafe
 *
 * Created on June 15, 2016, 8:28 PM
 */

#ifndef GRIDPOINT_H
#define GRIDPOINT_H

#include "ofMain.h"

class GridPoint : public ofVec3f {
public:

    float u;
    float v;

    GridPoint() {}
    
    void set(
            float x, float y, float z,
            float normx, float normy, float normz
            ) {
        ofVec3f::set(x, y, z);
        norm.set(normx, normy, normz);
        u = normx;
        v = normy;
    }

    void setUV(float w, float h) {
        u = norm.x * w;
        v = norm.y * h;
    }

    void setX(float h) {
        x = norm.x * h;
    }

    void setZ(float h) {
        z = norm.z * h;
    }

protected:

    ofVec3f norm;


};


#endif /* GRIDPOINT_H */

