/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   GridFace.h
 * Author: frankiezafe
 *
 * Created on June 15, 2016, 8:31 PM
 */

#ifndef GRIDFACE_H
#define GRIDFACE_H

#include "ofMain.h"
#include "GridPoint.h"

class GridFace {

public:
    
    GridPoint * pt1;
    GridPoint * pt2;
    GridPoint * pt3;
    ofVec3f center;
    ofVec3f normal;

    GridFace() {
        pt1 = nullptr;
        pt2 = nullptr;
        pt3 = nullptr;
    }
    
    void set( GridPoint * pt1, GridPoint * pt2, GridPoint * pt3 ) {
        this->pt1 = pt1;
        this->pt2 = pt2;
        this->pt3 = pt3;
        update();
    }

    void update() {

        center.x = (pt1->x + pt2->x + pt3->x) / 3;
        center.y = (pt1->y + pt2->y + pt3->y) / 3;
        center.z = (pt1->z + pt2->z + pt3->z) / 3;

        // calcul de la normal
        ofVec3f dir12(
                pt2->x - pt1->x,
                pt2->y - pt1->y,
                pt2->z - pt1->z
                );
        ofVec3f dir13(
                pt3->x - pt1->x,
                pt3->y - pt1->y,
                pt3->z - pt1->z
                );
        dir12.normalize();
        dir13.normalize();
        normal = dir12.cross(dir13);
        normal.normalize();

    }

};

#endif /* GRIDFACE_H */

