/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   MediaConfig.cpp
 * Author: frankiezafe
 * 
 * Created on June 18, 2016, 6:09 PM
 */

#include "MapConfig.h"

static MapConfig * _instance = nullptr;

MapConfig * MapConfig::get() {

    if ( _instance == nullptr ) {
        _instance = new MapConfig();
    }
    
    return _instance;

}

MapConfig::MapConfig() {

    up_red = 255;
    up_green = 255;
    up_blue = 185;

    down_red = 50;
    down_green = 50;
    down_blue = 50;
    
    grid_resolution = 50; 
    
    position_max = 200;
    
    z_range = 250;
    
    smoothing = 0.01;
    
    wave_ampl_min = 0.1;
    wave_ampl_max = 50;
    
    wave_freq_min = 0.01;
    wave_freq_max = 3;
    
    radius_min = 50;
    radius_max = 200;
    
    force_min = 0.01;
    radius_max = 0.02;
    
    chance_for_map = 0.8;
    rotation_mult = 0.1;
    
    display_debug = false;

}

MapConfig::~MapConfig() {}
